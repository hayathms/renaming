import sys
import os
import re
import random
import time

# This script removes blank space from all type of
# File names and replaces it with '-' recurssively
# This script also replaces '_' with '-'
# Running this script is easy rename.py 'path to fiolder'
#

class FilterFilenames(object):
    def __init__(self, filter_dir):
        self.filter_dir = filter_dir
    
    def start_process(self):
        for dirpath, dirnames, filenames in os.walk(self.filter_dir):
            self.dirpath = dirpath
            self.rename_files(filenames, dirpath)
            self.rename_folders(dirnames)
    
    def rename_files(self, filenames, dirpath):
        
        for file in filenames:
            
            name, ext = os.path.splitext(file)
            name = re.sub('\W', '_', name)
            
            while '__' in name:
                name = name.replace('__','_')
            
            if name == '' or name == '_':
                name = str(random.randint(1,2000))
            
            while name[-1] == '_':
                name = name[:-1]
            
            path = (os.path.join(dirpath,file))
            size_in_mb = os.stat(path).st_size/1000000.0 
            if size_in_mb < 1.0:
                continue
            
            if not ext and name == '_Directory':
                continue
            elif not ext:
                ext = input('give file extention for {0} :'.format(path))
            
            new_filename = name.title() + ext.lower()
            new_filename = new_filename
            
            new_path = os.path.join(self.dirpath, new_filename)
            old_path = os.path.join(self.dirpath, file)
            
            if new_path != old_path:
                os.rename(old_path, new_path)
                print(old_path)
                print(new_path)
    
    def rename_folders(self, dirnames):
        
        if not dirnames:
            return
        for folder_name in dirnames:
            
            name = re.sub('\W', '_', folder_name)
            
            while '__' in name:
                name = name.replace('__','_')
            
            while name[-1] == '_':
                name = name[:-1]
            
            new_foldername = name
            new_foldername = new_foldername.title()
            
            new_path = os.path.join(self.dirpath, new_foldername)
            old_path = os.path.join(self.dirpath, folder_name)
            
            if new_path != old_path:
                os.rename(old_path, new_path)
                print(old_path)
                print(new_path)
                
if __name__ == '__main__':
    
    path = sys.argv[1]
    for i in range(100):
        filter_filename = FilterFilenames(path)
        filter_filename.start_process()
        print('{0} Attempts - Will not spare until I change every file:'.format(i))
